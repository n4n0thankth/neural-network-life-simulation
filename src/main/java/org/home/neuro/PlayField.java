package org.home.neuro;

import java.util.*;
import java.util.stream.*;

import org.home.neuro.models.*;
import org.home.neuro.movehandlers.UnitToEmptyMoveHandler;
import org.home.neuro.movehandlers.UnitToFoodMoveHandler;
import org.home.neuro.strategies.*;

public class PlayField {
    private int gridWidth = 20;
    private int gridHeight = 10;
    private int lifetime = 30;
    private int totalFood = 6;
    private int totalUnits = 4;
    private int foodCount;
    private final Cell emptyCell = new Cell(new Empty());

    private volatile Cell[][] board;

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public int getTotalFood() {
        return totalFood;
    }

    public void setTotalFood(int totalFood) {
        this.totalFood = totalFood;
    }

    public int getFoodCount() {
        return foodCount;
    }

    public void setFoodCount(int foodCount) {
        this.foodCount = foodCount;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public PlayField() {
        this.board = new Cell[gridHeight][gridWidth];
    }

    public PlayField(int gridWidth, int gridHeight, int lifetime, int foodCount) {
        this.board = new Cell[gridHeight][gridWidth];
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.lifetime = lifetime;
        this.totalFood = foodCount;
    }

    public PlayField(int gridWidth, int gridHeight) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.board = new Cell[gridHeight][gridWidth];
    }

    public void initializeBoard() {
        for (int height = 0; height < gridHeight; height++) {
            for (int width = 0; width < gridWidth; width++) {
                board[width][height] = emptyCell;
            }
        }
        CellCoordinateGenerator cellCoordinateGenerator = new CellCoordinateGenerator(gridWidth,gridHeight);
        Iterator<Coordinates> nextCoordinatesIterator = cellCoordinateGenerator.getNextIterator();
        for (int i = 0; i < totalFood && nextCoordinatesIterator.hasNext(); i++) {
            Coordinates coord = nextCoordinatesIterator.next();
            Cell cell = new Cell(new Food(30,50));
            board[coord.getWidth()][coord.getHeight()] = cell;
        }
        Iterator<Coordinates> borderCoordinatesIterator = cellCoordinateGenerator.getNextBorderIterator();
        MovingStrategyHelper helper = new MovingStrategyHelper(this);
        for (int i = 0; i < totalUnits && borderCoordinatesIterator.hasNext(); i++) {
            Coordinates coord = borderCoordinatesIterator.next();
//            Cell cell = new MovingCell(new Unit(100), new RandomMovingStrategy(this, coord));
            Cell cell = new MovingCell(new Unit(500), new NearestFoodMovingStrategy(this, coord, helper));
            board[coord.getWidth()][coord.getHeight()] = cell;
        }

    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int height = 0; height < gridHeight; height++) {
            for (int width = 0; width < gridWidth; width++) {
                builder.append(" ").append(board[width][height]).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public synchronized void nextStep() {
//        Clear dead entities
        for (int width = 0; width < getGridWidth(); width++) {
            for (int height = 0; height < getGridHeight(); height++) {
                Cell cell = board[width][height];
                if (cell.getEntity().isDead()) {
                    makeCellEmpty(new Coordinates(width, height));
                }
            }
        }
        List<CellMove> list = Stream.of(board)
                .flatMap(Arrays::stream)
                .map(cell -> new CellMove(cell, Cell::takeTurn))
                .collect(Collectors.toList());
        list.forEach(CellMove::run);
    }

    public Coordinates moveCell(Coordinates coordinates, Direction direction) {
        Cell current = getCellByCoordinate(coordinates);
        Coordinates toCoord = coordinates.movedCoordinates(direction);
        if (isOutOfBound(toCoord)) {
            return coordinates;
        } else {
            Cell to = getCellByCoordinate(toCoord);

            if (current instanceof MovingCell) {
                if (current.isUnit()) {
                    if (to.isFood()) {
                        new UnitToFoodMoveHandler(this)
                                .handleMove(new GameItem<>(coordinates, (Unit) current.getEntity()), new GameItem<>(toCoord, (Food) to.getEntity()));
                        return toCoord;
                    }
                    if (to.isEmpty()) {
                        new UnitToEmptyMoveHandler(this)
                                .handleMove(new GameItem<>(coordinates, (Unit) current.getEntity()), new GameItem<>(toCoord, (Empty) to.getEntity()));
                        return toCoord;
                    }
                }
            }

            return coordinates;
        }
    }

    public Cell getCellByCoordinate(Coordinates coordinates) {
        return board[coordinates.getWidth()][coordinates.getHeight()];
    }

    public boolean isOutOfBound(Coordinates coordinates) {
        return coordinates.getWidth() > getGridWidth() - 1 || coordinates.getWidth() < 0
                || coordinates.getHeight() > getGridHeight() - 1 || coordinates.getHeight() < 0;
    }

    public void swapCells(Coordinates from, Coordinates to) {
        if (isOutOfBound(from) || isOutOfBound(to)) {
            return;
        }
        Cell buffer = board[from.getWidth()][from.getHeight()];
        board[from.getWidth()][from.getHeight()] = board[to.getWidth()][to.getHeight()];
        board[to.getWidth()][to.getHeight()] = buffer;
    }

    public void makeCellEmpty(Coordinates coordinates) {
        board[coordinates.getWidth()][coordinates.getHeight()] = emptyCell;
    }
}