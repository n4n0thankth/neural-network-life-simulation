package org.home.neuro;

import org.home.neuro.models.Entity;

public class GameItem<E extends Entity> {
    private final Coordinates coordinates;
    private final E entity;

    public GameItem(Coordinates coordinates, E entity) {
        this.coordinates = coordinates;
        this.entity = entity;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public E getEntity() {
        return entity;
    }
}
