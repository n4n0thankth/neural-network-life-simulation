package org.home.neuro;

import static javafx.scene.paint.Color.*;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import javafx.animation.*;
import javafx.application.*;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.*;
import org.home.neuro.models.*;

public class Main extends Application {

    private final Canvas canvas = new Canvas();
    private Stage stage;
    private GraphicsContext gc;

    private final int GRID_WIDTH = 20;
    private final int GRID_HEIGHT = 20;
    private final double VIEW_SCALE = 40;

    private volatile PlayField field;

    private volatile AtomicInteger turn = new AtomicInteger(0);

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        this.stage = primaryStage;
        stage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        final double winHeight = GRID_HEIGHT * VIEW_SCALE;
        final double winWidth = GRID_WIDTH * VIEW_SCALE;
        canvas.setHeight(winHeight);
        canvas.setWidth(winWidth);
        canvas.setVisible(true);

        Scene scene = new Scene(new StackPane(canvas), winWidth,winHeight);

        stage.setScene(scene);

        this.gc = canvas.getGraphicsContext2D();
        this.field = new PlayField(GRID_WIDTH,GRID_HEIGHT);
        field.setTotalFood(30);
        field.setLifetime(100);
        field.initializeBoard();
        new Thread(() -> {
            for (int i = 0; i <= field.getLifetime(); i++) {
                System.out.println("Iteration: " + i + " of " + field.getLifetime());
                field.nextStep();
                turn.incrementAndGet();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        AnimationTimer timer = new GameTimer();
        timer.start();

        stage.show();
    }

    private class GameTimer extends AnimationTimer {

        private volatile AtomicInteger timerTurn = new AtomicInteger(0);
        @Override
        public void handle(long now) {
            if (timerTurn.get() != turn.get()) {
                timerTurn.set(turn.get());
                gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                gc.setStroke(BLACK);

                for (int width = 0; width < field.getGridWidth(); width++) {
                    for (int height = 0; height < field.getGridHeight(); height++) {
                        Cell cell = field.getCellByCoordinate(new Coordinates(width, height));
                        String text = "";
                        if (cell.isEmpty()) {
                            gc.setFill(WHITE);
                        } else if (cell.isFood()) {
                            gc.setFill(RED);
                            text = ((Food) cell.getEntity()).getValue() + "\n" + ((Food) cell.getEntity()).getLifeTime();
                        } else if (cell.isUnit()) {
                            gc.setFill(BLUE);
                            text = String.valueOf(((Unit) cell.getEntity()).getHealth());
                        }
                        double x = width * VIEW_SCALE;
                        double y = height * VIEW_SCALE;
                        gc.fillRoundRect(x, y, VIEW_SCALE, VIEW_SCALE, VIEW_SCALE / 2, VIEW_SCALE / 2);
                        gc.setFill(BLACK);
                        gc.setStroke(BLACK);
                        gc.setFont(new Font("Times New Roman", VIEW_SCALE / 2));
                        gc.setTextAlign(TextAlignment.CENTER);
                        gc.fillText(text, x + VIEW_SCALE / 2, y + VIEW_SCALE / 2);
                    }
                }
            }
        }
    }
}