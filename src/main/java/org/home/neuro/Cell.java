package org.home.neuro;

import org.home.neuro.models.*;

public class Cell {
    private final Entity entity;

    Cell(Entity entity) {
        this.entity = entity;
    }

    public void takeTurn() {
        entity.takeTurn();
    }

    @Override
    public String toString() {
        return entity.toString();
    }

    public boolean isUnit() {
        return entity.isUnit();
    }

    public boolean isFood() {
        return entity.isFood();
    }

    public boolean isEmpty() {
        return entity.isEmpty();
    }

    public Entity getEntity() {
        return entity;
    }
}

