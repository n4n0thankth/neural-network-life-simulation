package org.home.neuro;

import org.home.neuro.models.Entity;
import org.home.neuro.strategies.MovingStrategy;

public class MovingCell extends Cell {

    private final MovingStrategy strategy;

    MovingCell(Entity entity, MovingStrategy strategy) {
        super(entity);
        this.strategy = strategy;
    }

    @Override
    public void takeTurn() {
        super.takeTurn();
        strategy.makeMove();
    }
}
