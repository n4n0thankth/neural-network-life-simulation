package org.home.neuro;

import java.util.*;


public class CellCoordinateGenerator {
    List<Coordinates> borders = new ArrayList<>();
    List<Coordinates> other = new ArrayList<>();
    Random random = new Random();

    public CellCoordinateGenerator(int width, int height) {
//        filling body
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                other.add(new Coordinates(j, i));
            }
        }

//        filling borders
        for (int i = 0; i < width - 1; i++) {
            borders.add(new Coordinates(0, i));
            borders.add(new Coordinates(height - 1, i));
        }
        for (int i = 1; i < height - 2; i++) {
            borders.add(new Coordinates(i, 0));
            borders.add(new Coordinates(i, width - 1));
        }
    }

    public Coordinates getNextBorder() throws Exception {
        return getNext(borders);
    }

    public Coordinates getNext() throws Exception {
        return getNext(other);
    }

    private Coordinates getNext(List<Coordinates> pairs) {
        if (!pairs.isEmpty()) {
//          Pair<Integer, Integer> result = pairs.get(random.nextInt() % (borders.size() - 1));
          Coordinates result = pairs.get(random.nextInt(pairs.size()-1));
            if (pairs.remove(result)) {
                return result;
            }
        }
        return null;
    }

    public Iterator<Coordinates> getNextIterator() {
        return new Iterator<Coordinates>() {
            @Override
            public boolean hasNext() {
                return !other.isEmpty();
            }

            @Override
            public Coordinates next() {
                return getNext(other);
            }
        };
    }

    public Iterator<Coordinates> getNextBorderIterator() {
        return new Iterator<Coordinates>() {
            @Override
            public boolean hasNext() {
                return !borders.isEmpty();
            }

            @Override
            public Coordinates next() {
                return getNext(borders);
            }
        };
    }
}

