package org.home.neuro;

import java.util.function.Consumer;

public class CellMove {
    private final Cell cell;
    private final Consumer<Cell> func;


    public CellMove(Cell cell, Consumer<Cell> func) {
        this.cell = cell;
        this.func = func;
    }

    public void run() {
        func.accept(cell);
    }
}
