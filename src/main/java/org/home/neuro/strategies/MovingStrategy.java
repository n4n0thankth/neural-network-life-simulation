package org.home.neuro.strategies;

import org.home.neuro.Coordinates;
import org.home.neuro.PlayField;

public abstract class MovingStrategy {
    protected PlayField field;
    protected Coordinates currentPosition;

    public MovingStrategy(PlayField field, Coordinates currentPosition) {
        this.field = field;
        this.currentPosition = currentPosition;
    }

    public abstract void makeMove();
}

