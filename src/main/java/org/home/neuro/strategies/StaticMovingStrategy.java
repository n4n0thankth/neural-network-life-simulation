package org.home.neuro.strategies;

import org.home.neuro.Coordinates;
import org.home.neuro.PlayField;

public class StaticMovingStrategy extends MovingStrategy {
    public StaticMovingStrategy(PlayField field, Coordinates currentPosition) {
        super(field, currentPosition);
    }

    @Override
    public void makeMove() {

    }
}
