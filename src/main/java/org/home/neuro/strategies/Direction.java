package org.home.neuro.strategies;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}