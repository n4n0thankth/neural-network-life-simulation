package org.home.neuro.strategies;

import static java.util.Collections.*;

import java.util.*;
import java.util.stream.*;

import org.home.neuro.*;
import org.home.neuro.models.*;

public class MovingStrategyHelper {

    PlayField playField;

    public MovingStrategyHelper(PlayField field) {
        this.playField = field;
    }

    public Optional<MoveToFood> nearestFoodMove(Coordinates fromPosition) {
        return Optional.ofNullable(getNearestFoodGameItem(fromPosition))
                .map(GameItem::getCoordinates)
                .map(foodCoord -> {
                    final List<Direction> directions = nearestPath(fromPosition, foodCoord);
                    return new MoveToFood(directions, foodCoord);
                });
    }

    public List<Direction> pathToExit(Coordinates currentPosition) {
        int nearestX = currentPosition.getWidth() - playField.getGridWidth() > currentPosition.getWidth()
                ? currentPosition.getWidth() : playField.getGridWidth();

        int nearestY = currentPosition.getHeight() - playField.getGridHeight() > currentPosition.getHeight()
                ? currentPosition.getHeight() : playField.getGridHeight();

        return nearestPath(currentPosition, new Coordinates(nearestX, nearestY));
    }

    public List<Direction> nearestPath(Coordinates from, Coordinates to) {
        final List<Direction> directions = new ArrayList<>();
        int vertical = to.getHeight() - from.getHeight();
        int horizontal = to.getWidth() - from.getWidth();

        if (vertical > 0) {
            Stream.iterate(0, i -> i).limit(Math.abs(vertical)).forEach(ignore -> directions.add(Direction.DOWN));
        } else if (vertical < 0) {
            Stream.iterate(0, i -> i).limit(Math.abs(vertical)).forEach(ignore -> directions.add(Direction.UP));
        }

        if (horizontal > 0) {
            Stream.iterate(0, i -> i).limit(Math.abs(horizontal)).forEach(ignore -> directions.add(Direction.RIGHT));
        } else if (horizontal < 0) {
            Stream.iterate(0, i -> i).limit(Math.abs(horizontal)).forEach(ignore -> directions.add(Direction.LEFT));
        }
        return directions;
    }

    public List<GameItem<Food>> getAllFoodGameItems() {
        List<GameItem<Food>> list = new ArrayList<>();
        for (int width = 0; width < playField.getGridWidth(); width++) {
            for (int height = 0; height < playField.getGridHeight(); height++) {
                Cell cell = playField.getCellByCoordinate(Coordinates.from(width, height));
                if (cell.isFood()) {
                    list.add(new GameItem<Food>(Coordinates.from(width, height), ((Food) cell.getEntity())));
                }
            }
        }
        return list;
    }

    public GameItem<Food> getNearestFoodGameItem(Coordinates fromPosition) {
        Comparator<Coordinates> comparator = new GameItemDistanceToComparator(fromPosition);
        return getAllFoodGameItems()
                .stream()
                .min((o1, o2) -> comparator.compare(o1.getCoordinates(), o2.getCoordinates()))
                .orElse(null);
    }

    public static class MoveToFood {
        private final List<Direction> directions;
        private Coordinates foodCellCoordinate;

        public MoveToFood(List<Direction> directions, Coordinates foodCellCoordinate) {
            this.directions = directions;
            this.foodCellCoordinate = foodCellCoordinate;
        }

        public List<Direction> getDirections() {
            return directions;
        }

        public Coordinates getFoodCellCoordinate() {
            return foodCellCoordinate;
        }
    }

    private class GameItemDistanceToComparator implements Comparator<Coordinates> {
        private final Coordinates to;

        public GameItemDistanceToComparator(Coordinates to) {
            this.to = to;
        }

        @Override
        public int compare(Coordinates o1, Coordinates o2) {
            return Double.compare(calcDistance(to, o1), calcDistance(to, o2));
        }

        private double calcDistance(Coordinates one, Coordinates two) {
            return Math.abs((one.getWidth() - two.getWidth()) ^ 2 - (one.getHeight() - two.getHeight()) ^ 2);
        }
    }
}
