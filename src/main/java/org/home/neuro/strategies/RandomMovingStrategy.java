package org.home.neuro.strategies;

import java.util.*;

import org.home.neuro.*;

public class RandomMovingStrategy extends MovingStrategy {
    private Random random = new Random();

    public RandomMovingStrategy(PlayField field, Coordinates currentPosition) {
        super(field, currentPosition);
    }

    @Override
    public void makeMove() {
        this.currentPosition = field.moveCell(currentPosition, Direction.values()[random.nextInt(4)]);
    }
}
