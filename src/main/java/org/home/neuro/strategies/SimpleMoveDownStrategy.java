package org.home.neuro.strategies;

import org.home.neuro.*;

public class SimpleMoveDownStrategy extends MovingStrategy{
    public SimpleMoveDownStrategy(PlayField field, Coordinates currentPosition) {
        super(field, currentPosition);
    }

    @Override
    public void makeMove() {
        this.currentPosition = field.moveCell(currentPosition, Direction.DOWN);
    }
}
