package org.home.neuro.strategies;

import java.util.*;

import org.home.neuro.*;

public class NearestFoodMovingStrategy extends MovingStrategy{
    private MovingStrategyHelper helper;
    private MovingStrategyHelper.MoveToFood moveToFood;
    private List<Direction> exit = new ArrayList<>();

    public NearestFoodMovingStrategy(PlayField field, Coordinates currentPosition, MovingStrategyHelper helper) {
        super(field, currentPosition);
        this.helper = helper;
        helper.nearestFoodMove(currentPosition).ifPresent(it -> moveToFood = it);
    }

    @Override
    public void makeMove() {
        if (exit.isEmpty()) {
            Optional.ofNullable(moveToFood).ifPresent(moveToFood1 -> {
                if (field.getCellByCoordinate(moveToFood1.getFoodCellCoordinate()).isFood()) {
                    if (!moveToFood1.getDirections().isEmpty()) {
                        this.currentPosition = field.moveCell(currentPosition, moveToFood1.getDirections().remove(0));
                    }
                } else {
                    helper.nearestFoodMove(currentPosition)
                            .ifPresentOrElse(it -> this.moveToFood = it, () -> exit = helper.pathToExit(currentPosition));
                }
            });
        } else {
            this.currentPosition = field.moveCell(currentPosition, exit.remove(0));
        }
    }
}
