package org.home.neuro.models;


import lombok.*;

@Getter
@Setter
public abstract class Entity {
    public abstract boolean isEmpty();
    public abstract boolean isFood();
    public abstract boolean isUnit();

    public abstract void takeTurn();
    public abstract boolean isDead();
}
