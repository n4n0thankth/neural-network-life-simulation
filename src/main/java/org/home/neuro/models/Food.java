package org.home.neuro.models;


import lombok.*;

@Data
public class Food extends Entity{
    private int value = 1;
    private int lifeTime = 1;

    public Food(int value, int lifeTime) {
        this.value = value;
        this.lifeTime = lifeTime;
    }

    @Override
    public String toString() {
        return "*";
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isFood() {
        return true;
    }

    @Override
    public boolean isUnit() {
        return false;
    }

    @Override
    public void takeTurn() {
        this.lifeTime -= 1;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean isDead() {
        return lifeTime < 0;
    }
}
