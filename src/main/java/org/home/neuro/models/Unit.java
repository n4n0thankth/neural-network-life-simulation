package org.home.neuro.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Unit extends Entity {
    private int health = 100;

    @Override
    public void takeTurn() {
        health -= 5;
    }

    @Override
    public boolean isDead() {
        return health <= 0;
    }

    @Override
    public String toString() {
        return "X";
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isFood() {
        return false;
    }

    @Override
    public boolean isUnit() {
        return true;
    }

    public void reduceHealth(int reduceOn) {
        health -= reduceOn;
    }

    public void increaseHealth(int increaseOn) {
        health += increaseOn;
    }


}
