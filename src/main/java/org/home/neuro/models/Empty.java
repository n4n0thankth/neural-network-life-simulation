package org.home.neuro.models;

public class Empty extends Entity{

    @Override
    public String toString() {
        return ".";
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public boolean isFood() {
        return false;
    }

    @Override
    public boolean isUnit() {
        return false;
    }

    @Override
    public void takeTurn() {
    }

    @Override
    public boolean isDead() {
        return false;
    }
}
