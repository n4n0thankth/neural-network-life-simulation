package org.home.neuro;

import lombok.Data;
import org.home.neuro.strategies.Direction;

@Data
public class Coordinates {
    int width;
    int height;

    public static Coordinates from(int x, int y) {
        return new Coordinates(x, y);
    }

    public Coordinates(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Coordinates movedCoordinates(Direction direction) {
        return switch (direction) {
            case UP -> new Coordinates(getWidth(), getHeight() - 1);
            case DOWN -> new Coordinates(getWidth(), getHeight() + 1);
            case LEFT -> new Coordinates(getWidth() - 1, getHeight());
            case RIGHT -> new Coordinates(getWidth() + 1, getHeight());
        };
    }
}
