package org.home.neuro.movehandlers;

import org.home.neuro.GameItem;
import org.home.neuro.PlayField;
import org.home.neuro.models.Empty;
import org.home.neuro.models.Unit;

public class UnitToEmptyMoveHandler extends CellMoveHandler<Unit, Empty> {

    public UnitToEmptyMoveHandler(PlayField playField) {
        this.playField = playField;
    }

    @Override
    public void handleMove(GameItem<Unit> from, GameItem<Empty> to) {
        playField.swapCells(from.getCoordinates(), to.getCoordinates());
    }
}
