package org.home.neuro.movehandlers;

import org.home.neuro.GameItem;
import org.home.neuro.PlayField;
import org.home.neuro.models.Unit;

public class UnitToUnitMoveHandler extends CellMoveHandler<Unit, Unit> {

    UnitToUnitMoveHandler(PlayField playField) {
        this.playField = playField;
    }

    @Override
    public void handleMove(GameItem<Unit> from, GameItem<Unit> to) {

    }
}
