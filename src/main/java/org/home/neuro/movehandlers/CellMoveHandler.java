package org.home.neuro.movehandlers;

import org.home.neuro.GameItem;
import org.home.neuro.PlayField;
import org.home.neuro.models.*;

public abstract class CellMoveHandler<FROM extends Entity, TO extends Entity> {
    PlayField playField;

    public abstract void handleMove(GameItem<FROM> from, GameItem<TO> to);
}

