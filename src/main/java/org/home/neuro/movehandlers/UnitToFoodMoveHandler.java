package org.home.neuro.movehandlers;

import org.home.neuro.GameItem;
import org.home.neuro.PlayField;
import org.home.neuro.models.Food;
import org.home.neuro.models.Unit;

public class UnitToFoodMoveHandler extends CellMoveHandler<Unit, Food> {

    public UnitToFoodMoveHandler(PlayField playField) {
        this.playField = playField;
    }

    @Override
    public void handleMove(GameItem<Unit> from, GameItem<Food> to) {
        from.getEntity().increaseHealth(to.getEntity().getValue());
        playField.swapCells(from.getCoordinates(), to.getCoordinates());
        playField.makeCellEmpty(from.getCoordinates());
    }
}
